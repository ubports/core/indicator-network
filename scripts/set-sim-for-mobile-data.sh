#!/bin/sh
dbus-send --session --print-reply           \
    --dest=com.lomiri.connectivity1         \
    /com/lomiri/connectivity1/Private       \
    org.freedesktop.DBus.Properties.Set     \
    string:com.lomiri.connectivity1.Private \
    string:SimForMobileData                 \
    variant:objpath:$1
/bin/sh ./get-sim-for-mobile-data.sh
