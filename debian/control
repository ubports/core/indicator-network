Source: lomiri-indicator-network
Section: gnome
Priority: optional
Maintainer: UBports Developers <developers@ubports.com>
Build-Depends: cmake,
               cmake-extras (>= 0.4),
               dbus,
               debhelper (>= 9.0.0),
               debhelper (>= 10) | dh-systemd,
               doxygen,
               googletest (>= 1.10),
               libgtest-dev,
               graphviz,
               intltool,
               libglib2.0-dev,
               libgmenuharness-dev (>= 0.1.1),
               libqofono-dev,
               libqofono-qt5-0,
               libqtdbusmock1-dev (>= 0.4),
               libqtdbustest1-dev,
               libsecret-1-dev,
               liblomiri-url-dispatcher-dev,
               liblomiri-api-dev,
               libnm-dev,
               ofono-dev,
               pkg-config,
               python3-all:native,
               python3-dbusmock (>= 0.16.3),
               qt5-default,
               qtbase5-dev,
               qtbase5-dev-tools,
               qtdeclarative5-dev,
               qtdeclarative5-dev-tools,
               qttools5-dev-tools,
               valgrind [amd64 armhf i386 powerpc],
               systemd,
Standards-Version: 3.9.5
Vcs-Git: https://gitlab.com/ubports/core/indicator-network.git
Vcs-Browser: https://gitlab.com/ubports/core/indicator-network
Homepage: https://gitlab.com/ubports/core/indicator-network
X-Ubuntu-Use-Langpack: yes

Package: lomiri-indicator-network
Architecture: any
Multi-Arch: foreign
Depends: ${misc:Depends},
         ${shlibs:Depends},
         indicator-common,
         network-manager,
         network-manager-openvpn,
         network-manager-pptp,
         suru-icon-theme,
Suggests: ofono,
          urfkill
Conflicts: chewie,
           indicators-client-plugin-network,
           indicator-network-autopilot,
           indicator-network-tools,
Replaces: indicator-network-autopilot,
          indicator-network-tools,
Provides: indicator-network-autopilot,
          indicator-network-tools,
Description: Systems settings menu service - Network indicator
 The Lomiri-indicator-network service is responsible for exporting the system settings
 menu through dbus.

Package: liblomiri-connectivity-qt1
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends},
Recommends: lomiri-indicator-network (>= ${source:Version}),
Description: Lomiri Connectivity Qt API
 Lomiri Connectivity API - Qt bindings

Package: liblomiri-connectivity-qt1-dev
Section: libdevel
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends},
         liblomiri-connectivity-qt1 (= ${binary:Version}),
         qtbase5-dev
Suggests: connectivity-doc,
Description: Lomiri Connectivity Qt API - development files
 Lomiri Connectivity API - Qt bindings
 .
 This package contains development files to develop against the Qt library.

Package: qml-module-lomiri-connectivity
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends},
         liblomiri-connectivity-qt1 (= ${binary:Version}),
Description: Lomiri Connectivity API QML Components
 Lomiri Connectivity API - QML bindings
 .
 This package contains the qtdeclarative bindings for Lomiri connectivity API.

Package: lomiri-connectivity-doc
Section: doc
Architecture: all
Depends: ${misc:Depends},
         ${shlibs:Depends},
Description: Lomiri Connectivity API - documentation
 Lomiri Connectivity API
 .
 This package contains developer documentation.
